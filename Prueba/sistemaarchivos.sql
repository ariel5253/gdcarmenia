/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.4.14-MariaDB : Database - sistemaarchivos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sistemaarchivos` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `sistemaarchivos`;

/*Table structure for table `registros` */

DROP TABLE IF EXISTS `registros`;

CREATE TABLE `registros` (
  `idRegistro` int(11) NOT NULL AUTO_INCREMENT,
  `proceso` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` date NOT NULL,
  `archivo` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idRegistro`),
  UNIQUE KEY `archivo` (`archivo`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `registros_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*Data for the table `registros` */

insert  into `registros`(`idRegistro`,`proceso`,`descripcion`,`fecha`,`archivo`,`estado`,`idUsuario`) values (8,'Incio del manual','Solo contiene una portada','2020-10-27','../Archivos/Estructura de la pagina de proyecto.pdf',1,1),(9,'Tabla de contenido','contiene en orden todas las abreviaturas ','2020-10-27','../Archivos/Reto hackathon 2020 - SenaSoft.pdf',1,6),(10,'Introduccion','Estoy en el proceso de la introduccion','2020-10-27','../Archivos/G13_TECNOLOGIA_LUCY_10.pdf',0,6),(11,'Diagramas','todos los diagramas ya estan realizados','2020-10-27','../Archivos/AutorizaciónUsodeImagen.pdf',1,1),(12,'Normativas y Resticciones','estoy trabajando en ello','2020-10-27','../Archivos/CertificadoDeFinalizacion_GitHub para programadores.pdf',0,5);

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `correo` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `celular` bigint(20) NOT NULL,
  `usuario` varchar(40) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*Data for the table `usuarios` */

insert  into `usuarios`(`idUsuario`,`nombre`,`correo`,`celular`,`usuario`,`password`) values (1,'Juan Silva','juancho29silva@gmail.com',3112119638,'Juan29silva','$argon2i$v=19$m=65536,t=4,p=1$RkdWNG5hYUNXaWxWNFNlTg$X7W88Trjl9vOxeb4QAdINPkUWSY/2er/WwHNNBn3XlY'),(5,'Jesus Ariel Gonzalez','Jesus@areil.com',3054256031,'JesusAriel','$argon2i$v=19$m=65536,t=4,p=1$d25qRnM2akZGUk5tL0o1Vw$2m7xu13SwP9n3kTDybWgkaNEW+6XmvWW0PVU0p4ePHo'),(6,'Lady Johanna Vera Torres','ljvera@armenia.gov.co',3214312022,'lady','$argon2i$v=19$m=65536,t=4,p=1$d25qRnM2akZGUk5tL0o1Vw$2m7xu13SwP9n3kTDybWgkaNEW+6XmvWW0PVU0p4ePHo');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
